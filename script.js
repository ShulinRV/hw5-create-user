// Опишіть своїми словами, що таке метод об'єкту
// Метод это свойства функии обьекта
// Який тип даних може мати значення властивості об'єкта?
// null, object, array
// Об'єкт це посилальний тип даних. Що означає це поняття?
// Завдання

// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

let firstName = prompt("First name?");
let lastName = prompt("Last name?");
let birthDay = prompt("Type your date of birthday in format 'DD.MM.YYYY' ");

function createNewUser(firstName, lastName, birthDay) {
  const user = {
    firstName: firstName,
    lastName: lastName,
    birthDay: birthDay,
    get name() {
      return this.firstName[0].toLowerCase();
    },
    get lastNameIs() {
      return this.lastName.toLowerCase();
    },
    getName: function () {
      return console.log(
        this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
      );
    },
    getAge: function () {
      const [day, month, year] = birthDay.split(".");

      const date = new Date();
      date.setFullYear(year);
      date.setMonth(month - 1);
      date.setDate(day);
      const now = new Date();
      return Math.floor((now - date) / 1000 / 31536000);
    },
    getPassword: function () {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthDay.charAt(6) +
        this.birthDay.charAt(7) +
        this.birthDay.charAt(8) +
        this.birthDay.charAt(birthDay.length - 1)
      );
    },
  };

  return user;
}

let newUser = new createNewUser(firstName, lastName, birthDay);
newUser.getName();
console.log(newUser.getPassword());
console.log(newUser.getAge());
console.log(newUser);
